"""A set of helper functions."""

from copy import deepcopy
import logging
import fiona
from shapely.geometry import shape, Polygon, MultiPolygon
from shapely.strtree import STRtree


def get_features(path):
    """Load Fiona features and their shared metadata from a file.

    Tries to open the file at path with Fiona, gets all the features from it at
    once as well as the meta information. Returns both.

    Args:
        path (str): Path to Fiona-compatible file to get features from.

    Returns:
        tuple: Two elements, the features and their metadata dictionary.

    """
    with fiona.open(path, "r") as source:
        features = list(source)
        meta = source.meta

    return features, meta


def to_file(features, meta, attribute, output, driver):
    """Save Fiona features to a file.

    Will *overwrite* existing files!

    Args:
        features: (sequence) Fiona features
        meta: (dict) Fiona meta
        attribute: (str) Attribute name
        output: (str) Relative or absolute path to file
        driver: (str) GDAL name of driver for writing
    """
    # add the new field to the schema
    meta["schema"]["properties"][attribute] = "float"

    logging.info(f"Writing features to {output}...")

    with fiona.open(
        output, "w", driver=driver, crs=meta["crs"], schema=meta["schema"]
    ) as output_file:

        output_file.writerecords(features)

    logging.info(f"Writing features... Done!")


def get_numeric_attributes(path):
    """Extract all numeric attributes from a Fiona/GDAL compatible file.

    Args:
        path (str): Path to Fiona-compatible file to get features from.

    Returns:
        list: The names of the attributes

    """
    with fiona.open(path, "r") as source:
        schema = source.schema

    attributes = []
    for attribute, attribute_type in schema["properties"].items():
        # if there are length limititations, fiona appends them to the name of
        # the attribute like "str:254" or "float:24.15" or "int:10"
        attribute_type = attribute_type.split(":")[0]

        if fiona.FIELD_TYPES_MAP[attribute_type] in (int, float):
            attributes.append(attribute)

    return attributes


def make_geometries(features, validate=True):
    """Create a Shapely geometry for each feature.

    The geometries are returned in the same order as the original
    list of features.

    If validate is set to True, the geometries will be tested for
    validity and repaired if possible.

    Args:
        features (sequence): A sequence of Fiona features.
        validate (bool): A boolean flag indicating if validation should be attempted.

    Returns:
        list: Shapely geometries, one for each Fiona feature in the input.

    """
    geometries = [shape(f["geometry"]) for f in features]

    if validate:
        geometries = [make_valid(g) for g in geometries]

    return geometries


def make_valid(geometry):
    """Check a Shapely geometry for validity and try to fix errors via buffer(0).

    Only supports polygonal geometries (Polygon and MultiPolygon). Other geometries
    are returned unchecked and unchanged.

    Args:
        geometry: A Shapely geometry.

    Returns:
        A valid Shapely geometry if the input was already valid or could be fixed.

    """
    if not isinstance(geometry, (Polygon, MultiPolygon)):
        return geometry

    if not geometry.is_valid:
        logging.warning("Trying to fix invalid geometry...")
        geometry = geometry.buffer(0)

        if not geometry.is_valid:
            logging.critical(
                "Invalid geometry could not be fixed, expect errorneous output!"
            )
            logging.debug(geometry)

    return geometry


def generate_strtree(geometries):
    """Build a "numbered" STRtree index for a sequence of Shapely geometries.

    Adds an sequence index attribute `i` to each geometry before building an
    STRTree for the geometries which allows for identifying the geometries after
    querying the STRTree later. This is especially handy if your geometries are
    actually "parts" of other data, like for example Fiona features.
    See https://github.com/Toblerity/Shapely/issues/618#issuecomment-408891713

    The original geometries are not mutated, instead this function operates on a
    deep copy of them.

    Args:
        geometries (sequence): Shapely geometries to build a STRTree with.

    Returns:
        A STRtree index of the geometries.

    """
    geometries_numbered = []
    for i, geometry in enumerate(geometries):
        geometry = deepcopy(geometry)  # let's not mutate the originals
        geometry.i = i
        geometries_numbered.append(geometry)

    return STRtree(geometries_numbered)
