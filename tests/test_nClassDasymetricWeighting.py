import os
import sys
import unittest
import cogran

# insert the directory in path
sys.path.insert(0, "../")
sys.path.append(os.getcwd())


class TestCoGranNClassDasymetricWeighting(unittest.TestCase):
    # def test_ncdw_is_not_ncdw_rel(self):
    # self.assertIsNot(ncdw, ncdw_rel)

    def test_NClassDasymetricWeighting_target_feature_Pt_absolute_values(self):
        source = (
            "tests/nClassDasymetricWeighting/"
            "nClassDasymetricWeighting_source.geojson"
        )
        target = (
            "tests/nClassDasymetricWeighting/"
            "nClassDasymetricWeighting_target.geojson"
        )
        mask = (
            "tests/nClassDasymetricWeighting/"
            "nClassDasymetricWeighting_control.geojson"
        )
        target_features, _ = cogran.execute_cogran(
            source=source,
            target=target,
            mask=mask,
            attribute="abs_val",
            weight="weight",
            function=cogran.nclass_dasymetric_weighting,
        )
        self.assertAlmostEqual(target_features[0]["properties"]["abs_val"], 3, places=1)
        self.assertAlmostEqual(target_features[1]["properties"]["abs_val"], 3, places=1)
        self.assertAlmostEqual(target_features[2]["properties"]["abs_val"], 1, places=1)
        self.assertAlmostEqual(target_features[3]["properties"]["abs_val"], 1, places=1)

    # def test_NClassDasymetricWeighting_target_feature_Pt_relative_values(self):
    # pass


if __name__ == "__main__":
    unittest.main()
