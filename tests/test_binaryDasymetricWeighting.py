import os
import sys
import unittest
import cogran

# insert the directory in path
sys.path.insert(0, "../")
sys.path.append(os.getcwd())


class TestCoGranDasymetricWeighting(unittest.TestCase):
    def test_binaryDasymetricWeighting_target_feature_Pt_absolute_values(self):
        source = (
            "tests/binaryDasymetricWeighting/"
            "binaryDasymetricWeighting_source.geojson"
        )
        target = (
            "tests/binaryDasymetricWeighting/"
            "binaryDasymetricWeighting_target.geojson"
        )
        mask = (
            "tests/binaryDasymetricWeighting/"
            "binaryDasymetricWeighting_control.geojson"
        )

        target_features, _ = cogran.execute_cogran(
            source=source,
            target=target,
            mask=mask,
            attribute="abs_val",
            function=cogran.binary_dasymetric_weighting,
        )

        self.assertAlmostEqual(target_features[0]["properties"]["abs_val"], 0)
        self.assertAlmostEqual(target_features[1]["properties"]["abs_val"], 3)
        self.assertAlmostEqual(target_features[2]["properties"]["abs_val"], 3)
        self.assertAlmostEqual(target_features[3]["properties"]["abs_val"], 1)

    # def test_binaryDasymetricWeighting_target_feature_Pt_relative_values(self):
    #     pass


if __name__ == "__main__":
    unittest.main()
